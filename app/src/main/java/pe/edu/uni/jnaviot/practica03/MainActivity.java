package pe.edu.uni.jnaviot.practica03;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.widget.Button;
import android.widget.RadioButton;

import com.google.android.material.snackbar.Snackbar;

public class MainActivity extends AppCompatActivity {
    RadioButton radioButton1, radioButton2, radioButton3;
    Button button;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        radioButton1 = findViewById(R.id.radio_button_1);
        radioButton2 = findViewById(R.id.radio_button_2);
        radioButton3 = findViewById(R.id.radio_button_3);
        button = findViewById(R.id.button_main);

        Intent intent = new Intent(MainActivity.this, CalculatorActivity.class);

        button.setOnClickListener(view -> {
            Resources res = getResources();
            if(radioButton1.isChecked()){
                intent.putExtra("BASIC",true); // boolean
                startActivity(intent);
            }
            else if(radioButton2.isChecked()){
                //Dialogo
                //Dialogo
                AlertDialog.Builder builder  = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle(R.string.dialog_title);
                builder.setCancelable(false);
                builder.setMessage(R.string.question);
                builder.setPositiveButton("Si", (dialogInterface, i) -> {

                });
                builder.setNegativeButton("No", (dialogInterface, i) -> {
                    moveTaskToBack(true);
                    android.os.Process.killProcess(android.os.Process.myPid());
                    System.exit(1);
                });
                builder.create().show();
            }
            else if(radioButton3.isChecked()){
                //Dialogo
                AlertDialog.Builder builder  = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle(R.string.dialog_title);
                builder.setCancelable(false);
                builder.setMessage(R.string.question);
                builder.setPositiveButton("Si", (dialogInterface, i) -> {

                });
                builder.setNegativeButton("No", (dialogInterface, i) -> {
                    moveTaskToBack(true);
                    android.os.Process.killProcess(android.os.Process.myPid());
                    System.exit(1);
                });
                builder.create().show();

            }
            else{
                Snackbar.make(view, R.string.snackbar_msg, Snackbar.LENGTH_LONG).show();
                return;
            }


        });
    }
}