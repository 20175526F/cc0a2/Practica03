package pe.edu.uni.jnaviot.practica03;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class CalculatorActivity extends AppCompatActivity {

    Button n1,n2,n3,n4,n5,n6,n7,n8,n9,n0,oSuma,oResta,oMultiplica,oDivide,oBorra,oEnter;
    TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator);

        n1 = findViewById(R.id.button_1);
        n2 = findViewById(R.id.button_2);
        n3 = findViewById(R.id.button_3);
        n4 = findViewById(R.id.button_4);
        n5 = findViewById(R.id.button_5);
        n6 = findViewById(R.id.button_6);
        n7 = findViewById(R.id.button_7);
        n8 = findViewById(R.id.button_8);
        n9 = findViewById(R.id.button_9);
        n0 = findViewById(R.id.button_0);

        textView = findViewById(R.id.text_view_calc);

        oSuma = findViewById(R.id.button_plus);
        oResta = findViewById(R.id.button_minus);
        oMultiplica = findViewById(R.id.button_dot);
        oDivide = findViewById(R.id.button_div);

        oBorra= findViewById(R.id.button_erase);
        oEnter= findViewById(R.id.button_enter);

        StringBuilder term1 = new StringBuilder();
        StringBuilder term2 = new StringBuilder();
        boolean flag = false;

        n1.setOnClickListener(view -> {
            term1.append("1");
            textView.setText(term1.toString());
        });
        n2.setOnClickListener(view -> {
            term1.append("2");
            textView.setText(term1.toString());
        });
        n3.setOnClickListener(view -> {
            term1.append("3");
            textView.setText(term1.toString());
        });
        n4.setOnClickListener(view -> {
            term1.append("4");
            textView.setText(term1.toString());
        });
        n5.setOnClickListener(view -> {
            term1.append("5");
            textView.setText(term1.toString());
        });
        n6.setOnClickListener(view -> {
            term1.append("6");
            textView.setText(term1.toString());
        });
        n7.setOnClickListener(view -> {
            term1.append("7");
            textView.setText(term1.toString());
        });
        n8.setOnClickListener(view -> {
            term1.append("8");
            textView.setText(term1.toString());
        });
        n9.setOnClickListener(view -> {
            term1.append("9");
            textView.setText(term1.toString());
        });
        n0.setOnClickListener(view -> {
            term1.append("0");
            textView.setText(term1.toString());
        });

        oBorra.setOnClickListener(view -> {
            term1.setLength(term1.length()-1);
            textView.setText(term1.toString());
        });

        oSuma.setOnClickListener(view -> {
            int s1 = Integer.parseInt(term1.toString());
            textView.setText("");

        });

        oResta.setOnClickListener(view -> {
            int s1 = Integer.parseInt(term1.toString());
            textView.setText("");

        });

        oMultiplica.setOnClickListener(view -> {
            int s1 = Integer.parseInt(term1.toString());
            textView.setText("");

        });

        oDivide.setOnClickListener(view -> {
            int s1 = Integer.parseInt(term1.toString());
            textView.setText("");

        });


    }
}